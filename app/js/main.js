//---data of modal---
// terminal,
// delivery
// swift
// withdrow
// payment-methords
// sms-code
// thanks
// description
// add-method


let openModal = document.querySelector('#openModal');
let closeModal = document.querySelectorAll('.modal-w__close');
let iconInfo = document.querySelector('.icon-info');
let addNewMethod = document.querySelector('#add-method');
let addNewMethodSingle = document.querySelector('#add-method-single');
let modals = document.querySelectorAll('.modal-w-dialog');
let withdrow = document.querySelector('#withdrow');
let modalWithdrow = document.querySelector('#modal-withdrow');
let delivery = document.querySelector('#delivery');
let swift = document.querySelector('#swift');
let payment_methords = document.querySelector('#payment-methords');
let thanks = document.querySelector('#thanks');
let bgModal = ['rgba(0,0,0,.05)', 'rgba(255,255,255,1)'];
let inputCheck = '';

//modalName - имя модалки
//modalData - дата (написана выше)
//bgType - фон

function singleModal(modalName, modalData, bgType = bgModal[0]) {
  modalName.addEventListener('click', () => {
    modals.forEach(i => {
      if (i.dataset.modal === modalData) {
        i.style.display = 'block';
        openModal.style.display = 'block';
        openModal.style.background = bgType;
      } else {
        i.style.display = 'none';
      }
    })
  })
}

singleModal(withdrow, 'withdrow',);
singleModal(modalWithdrow, 'sms-code');
singleModal(iconInfo, 'description',bgModal[1]);
singleModal(addNewMethod, 'add-method');


document.querySelectorAll('.input-add-method').forEach((i) => {
  i.addEventListener('click', () =>{
    if (i.checked) {
      inputCheck = i.id;
    }

    switch (inputCheck) {
      case 'add-swift-by':
        singleModal(addNewMethodSingle, 'swift', bgModal[1]);
        break;
      case 'add-courier-by':
        singleModal(addNewMethodSingle, 'delivery', bgModal[1]);
        break;
      case 'add-terminal-by':
        singleModal(addNewMethodSingle, 'terminal', bgModal[1]);
        break;
    }
  })
})

addNewMethodSingle.addEventListener('click', (e) => {
  e.preventDefault();
  inputCheck = '';
})


closeModal.forEach((i) => {
  i.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector('#openModal').style.display = 'none';
  })
})

